import java.util.Scanner;

public class Player{
    private int playerType; //1 or 2

    //constructor
    public Player(){
        playerType = 1;
    }

    public int makeMove(){
        System.out.print("It's your turn. Please, enter desired index from 1 to 9: ");
        Scanner input = new Scanner(System.in);
        return input.nextInt();
    }

    public void changePlayer(){
        if(playerType == 1)
            playerType = 2;
        else
            playerType = 1;
    }

    public int getType(){
        return playerType;
    }

}

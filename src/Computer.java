public class Computer{

    //constructor
    public Computer(){}

    public int makeMove(GameBoard board){
        int position = findOptimalMove(board);//index selected by the computer
        System.out.print("Computer make a move on the cell with number: " + position +"\n");
        return position;
    }


    //Implementing minimax algorithm
    public static int minimax(GameBoard board, int depth, boolean isMax){

        if(board.win() == 2){
            return 10;
        }
        if(board.win() == 1){
            return -10;
        }
        if(!board.hasSpace())
            return 0;

        int best;
        // If this maximizer's move
        if(isMax){
            best = -1000;
            // Traverse all cells
            for(int i=0; i<3;i++){
                for(int j=0; j<3; j++){
                    // Check if cell is empty
                    if(board.getElement(i,j) == ' '){
                        // Make the move
                        board.putElement(i,j, 'O');
                        // Call minimax recursively and choose the maximum value
                        best = Math.max(best, minimax(board, depth+1, false));
                        // Undo the move
                        board.putElement(i, j, ' ');
                    }
                }
            }
        }
        // If this minimizer's move
        else{
            best = 1000;
            // Traverse all cells
            for(int i=0; i<3;i++){
                for(int j=0; j<3; j++){
                    // Check if cell is empty
                    if(board.getElement(i,j) == ' '){
                        // Make the move
                        board.putElement(i,j, 'X');
                        // Call minimax recursively and choose the minimum value
                        best = Math.min(best, minimax(board, depth+1, true));
                        // Undo the move
                        board.putElement(i, j, ' ');
                    }
                }
            }
        }
        return best;

    }
    // This will return the best possible move for the player
    public static int findOptimalMove(GameBoard board){
        int bestMoveVal = -1000;
        int bestMoveRow = -1;
        int bestMoveCol = -1;

        // Traverse all cells, evaluate minimax function
        // for all empty cells. And return the cell
        // with optimal value.
        for(int i=0; i<3; i++){
            for(int j=0; j<3; j++){
                // Check if cell is empty
                if(board.getElement(i,j) == ' '){
                    // Make the move
                    board.putElement(i,j, 'O');
                    // compute evaluation function for this move.
                    int currentMoveVal = minimax(board, 0, false);
                    // Undo the move
                    board.putElement(i, j, ' ');
                    // If the value of the current move is more than the best value, then update best
                    if(currentMoveVal > bestMoveVal){
                        bestMoveRow = i;
                        bestMoveCol = j;
                        bestMoveVal = currentMoveVal;
                    }
                }
            }
        }
        //converting row and column to index format
        int i = bestMoveRow;
        int j = bestMoveCol;

        if(i==0 && j==0) return 1;
        if(i==0 && j==1) return 2;
        if(i==0 && j==2) return 3;
        if(i==1 && j==0) return 4;
        if(i==1 && j==1) return 5;
        if(i==1 && j==2) return 6;
        if(i==2 && j==0) return 7;
        if(i==2 && j==1) return 8;
        if(i==2 && j==2) return 9;
        return 0;
    }


}
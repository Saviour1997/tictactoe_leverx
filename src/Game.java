import java.util.Scanner;

public class Game{

        private int moves;  //count of the total moves played

    //constructor
    public Game(){

        moves = 0;
    }

    public void startGame(){

        System.out.println("1.Enter 1 to play with other Player:" + "\n"+ "2.Enter 2 to play with PC: ");
        Scanner input = new Scanner(System.in);
        int choice = input.nextInt();

        Player currentPlayer = new Player();
        Computer computer = new Computer();

        GameBoard board = new GameBoard();
        System.out.println("Welcome to the Game! You can see how numbers are followed at the game board:");
        board.printIndexBoard();

        while(board.win() == 0 && moves < 9){
            int index = 0;
            if(choice == 1 || currentPlayer.getType() == 1)
                index = currentPlayer.makeMove();
            else
                index = computer.makeMove(board);

            if(board.registerMove(currentPlayer.getType(), index) == 1){
                moves++;
                currentPlayer.changePlayer();
            }
            else
                System.out.println("Enter a valid index.");     //invalid index, move not registered
            board.printBoard();

        }

        if(moves < 9){      //not a Draw
            if(board.win() == 1)
                System.out.println("Player1 Wins!");
            else{           //player2
                if(choice == 1)
                    System.out.println("Player2 Wins!");
                else
                    System.out.println("Computer Wins!");
            }
        }

        if(moves == 9 && board.win() == 0)      //Draw
            System.out.println("Result of a game -- Draw!");
    }

}
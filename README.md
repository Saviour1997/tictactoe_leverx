Classic Tic-tac-toe game.

-> Run Launcher.java

->Follow the console. This game have two versions to play: 1.Player vs Player. 2. Player vs PC. Enter 1 or 2  to start the game.

->Play the game by entering integer index values. The program ends when the game is won by player(3 in a row/column/diagonal), or the game is a draw.

For the Player vs PC version, Computer's playing strategy is designed by an implementation of the Minimax Algorithm, which basically works by backtracking and determining the moves for maximum and minimum utility.